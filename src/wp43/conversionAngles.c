// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "conversionAngles.h"

#include "constantPointers.h"
#include "debug.h"
#include "error.h"
#include "fonts.h"
#include "mathematics/comparisonReals.h"
#include "registers.h"
#include "registerValueConversions.h"

#include "wp43.h"

void fnCvtToCurrentAngularMode(uint16_t fromAngularMode) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), fromAngularMode, currentAngularMode);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
      break;
    }

    case dtReal34: {
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), fromAngularMode, currentAngularMode);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a long integer, a real34 or an angle34\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtFromCurrentAngularMode(uint16_t toAngularMode) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);

      if(currentAngularMode == amMultPi && getRegisterAngularMode(REGISTER_X) == amNone) {
        real_t x;
        real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
        realMultiply(&x, const_pi, &x, &ctxtReal39);
        convertRealToReal34ResultRegister(&x, REGISTER_X);
        //setRegisterAngularMode(REGISTER_X, toAngularMode);
        //break;
      }

      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), currentAngularMode, toAngularMode);
      setRegisterAngularMode(REGISTER_X, toAngularMode);
      break;
    }

    case dtReal34: {
      if(currentAngularMode == amDMS && getRegisterAngularMode(REGISTER_X) == amNone) {
        real34FromDmsToDeg(REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
        setRegisterAngularMode(REGISTER_X, amDegree);
      }

      if(currentAngularMode == amMultPi && getRegisterAngularMode(REGISTER_X) == amNone) {
        real_t x;
        real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
        realMultiply(&x, const_pi, &x, &ctxtReal39);
        convertRealToReal34ResultRegister(&x, REGISTER_X);
        //setRegisterAngularMode(REGISTER_X, toAngularMode);
        //break;
      }

      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), getRegisterAngularMode(REGISTER_X) == amNone ? currentAngularMode : getRegisterAngularMode(REGISTER_X), toAngularMode);
      setRegisterAngularMode(REGISTER_X, toAngularMode);
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a long integer, a real34 or an angle16 or an angle34\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtDegToRad(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDegree, amRadian);
      setRegisterAngularMode(REGISTER_X, amRadian);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amDegree || getRegisterAngularMode(REGISTER_X) == amDMS || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDegree, amRadian);
        setRegisterAngularMode(REGISTER_X, amRadian);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged degree as an input of fnCvtDegToRad");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtRadToDeg(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amRadian, amDegree);
      setRegisterAngularMode(REGISTER_X, amDegree);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amRadian || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amRadian, amDegree);
        setRegisterAngularMode(REGISTER_X, amDegree);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged degree as an input of fnCvtRadToDeg");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtMultPiToRad(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, amRadian);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amNone) {
        real34FromDmsToDeg(REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
        setRegisterAngularMode(REGISTER_X, amRadian);
      }
      else if(getRegisterAngularMode(REGISTER_X) == amMultPi) {
        setRegisterAngularMode(REGISTER_X, amRadian);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged mult" STD_pi " as an input of fnCvtMultPiToRad");
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
    }
  }
}



void fnCvtRadToMultPi(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, amMultPi);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amRadian || getRegisterAngularMode(REGISTER_X) == amNone) {
        setRegisterAngularMode(REGISTER_X, amMultPi);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged radian as an input of fnCvtRadToMultPi");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtDegToDms(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, amDMS);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amDegree || getRegisterAngularMode(REGISTER_X) == amNone) {
        setRegisterAngularMode(REGISTER_X, amDMS);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged degree as an input of fnCvtDegToDms");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtDmsToDeg(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, amDegree);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amNone) {
        real34FromDmsToDeg(REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
        setRegisterAngularMode(REGISTER_X, amDegree);
      }
      else if(getRegisterAngularMode(REGISTER_X) == amDMS) {
        setRegisterAngularMode(REGISTER_X, amDegree);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged d.ms as an input of fnCvtDmsToDeg");
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
    }
  }
}



void fnCvtDegToMil(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDegree, amMil);
      setRegisterAngularMode(REGISTER_X, amMil);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amDegree || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDegree, amMil);
        setRegisterAngularMode(REGISTER_X, amMil);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged degree as an input of fnCvtDegToMil");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtMilToDeg(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amMil, amDegree);
      setRegisterAngularMode(REGISTER_X, amDegree);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amMil || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amMil, amDegree);
        setRegisterAngularMode(REGISTER_X, amDegree);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged mil as an input of fnCvtMilToDeg");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtMilToRad(uint16_t unusedButMandatoryParameter){
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amMil, amRadian);
      setRegisterAngularMode(REGISTER_X, amRadian);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amMil || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amMil, amRadian);
        setRegisterAngularMode(REGISTER_X, amRadian);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged mil as an input of fnCvtMilToRad");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtRadToMil(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amRadian, amMil);
      setRegisterAngularMode(REGISTER_X, amMil);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amRadian || getRegisterAngularMode(REGISTER_X) == amNone) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amRadian, amMil);
        setRegisterAngularMode(REGISTER_X, amMil);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged radian as an input of fnCvtRadToMil");
        return;
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
      return;
    }
  }
}



void fnCvtDmsToCurrentAngularMode(uint16_t unusedButMandatoryParameter) {
  if(!saveLastX()) {
    return;
  }

  switch(getRegisterDataType(REGISTER_X)) {
    case dtLongInteger: {
      convertLongIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
      convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDMS, currentAngularMode);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
      break;
    }

    case dtReal34: {
      if(getRegisterAngularMode(REGISTER_X) == amNone) {
        real34FromDmsToDeg(REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDMS, currentAngularMode);
        setRegisterAngularMode(REGISTER_X, currentAngularMode);
      }
      else if(getRegisterAngularMode(REGISTER_X) == amDMS) {
        convertAngle34FromTo(REGISTER_REAL34_DATA(REGISTER_X), amDMS, currentAngularMode);
        setRegisterAngularMode(REGISTER_X, currentAngularMode);
      }
      else {
        displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot use an angle34 not tagged d.ms as an input of fnCvtDmsToDeg");
      }
      break;
    }

    default: {
      displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("the input value must be a real34 or a long integer\n%s cannot be converted to an angle!", getRegisterDataTypeName(REGISTER_X, true, false));
    }
  }
}



void convertAngle34FromTo(real34_t *angle34, angularMode_t fromAngularMode, angularMode_t toAngularMode) {
  real_t angle;

  real34ToReal(angle34, &angle);
  convertAngleFromTo(&angle, fromAngularMode, toAngularMode, &ctxtReal39);
  realToReal34(&angle, angle34);
}



void convertAngleFromTo(real_t *angle, angularMode_t fromAngularMode, angularMode_t toAngularMode, realContext_t *realContext) {
  switch(fromAngularMode) {
    case amRadian:
    case amMultPi: {
      switch(toAngularMode) {
        case amGrad: {
          realMultiply(angle, const_200onPi,  angle, realContext);
          break;
        }
        case amDegree:
        case amDMS: {
          realMultiply(angle, const_180onPi,  angle, realContext);
          break;
        }
        case amMil: {
          realMultiply(angle, const_3200onPi, angle, realContext);
          break;
        }
        default: {
        }
      }
      break;
    }

    case amGrad: {
      switch(toAngularMode) {
        case amRadian:
        case amMultPi: {
          realDivide(  angle, const_200onPi, angle, realContext);
          break;
        }
        case amDegree:
        case amDMS: {
          realMultiply(angle, const_9on10,   angle, realContext);
          break;
        }
        case amMil: {
          realMultiply(angle, const_16,      angle, realContext);
          break;
        }
        default: {
        }
      }
      break;
    }

    case amDegree:
    case amDMS: {
      switch(toAngularMode) {
        case amRadian:
        case amMultPi: {
          realDivide(  angle, const_180onPi, angle, realContext);
          break;
        }
        case amGrad: {
          realDivide(  angle, const_9on10,   angle, realContext);
          break;
        }
        case amMil: {
          realDivide(  angle, const_9on160,  angle, realContext);
          break;
        }
        default: {
        }
      }
      break;
    }

    case amMil: {
      switch(toAngularMode) {
        case amRadian:
        case amMultPi: {
          realDivide(  angle, const_3200onPi, angle, realContext);
          break;
        }
        case amGrad: {
          realDivide(  angle, const_16,       angle, realContext);
          break;
        }
        case amDegree:
        case amDMS: {
          realMultiply(angle, const_9on160,   angle, realContext);
          break;
        }
        default: {
        }
      }
      break;
    }

    default: {
    }
  }
}



void checkDms34(real34_t *angle34Dms) {
  int16_t  sign;
  real_t angleDms, degrees, minutes, seconds;

  real34ToReal(angle34Dms, &angleDms);

  sign = realIsNegative(&angleDms) ? -1 : 1;
  realSetPositiveSign(&angleDms);

  realToIntegralValue(&angleDms, &degrees, DEC_ROUND_DOWN, &ctxtReal39);
  realSubtract(&angleDms, &degrees, &angleDms, &ctxtReal39);

  realMultiply(&angleDms, const_100, &angleDms, &ctxtReal39);
  realToIntegralValue(&angleDms, &minutes, DEC_ROUND_DOWN, &ctxtReal39);
  realSubtract(&angleDms, &minutes, &angleDms, &ctxtReal39);

  realMultiply(&angleDms, const_100, &seconds, &ctxtReal39);

  if(realCompareGreaterEqual(&seconds, const_60)) {
    realSubtract(&seconds, const_60, &seconds, &ctxtReal39);
    realAdd(&minutes, const_1, &minutes, &ctxtReal39);
  }

  if(realCompareGreaterEqual(&minutes, const_60)) {
    realSubtract(&minutes, const_60, &minutes, &ctxtReal39);
    realAdd(&degrees, const_1, &degrees, &ctxtReal39);
  }

  realDivide(&minutes, const_100, &minutes, &ctxtReal39);
  realAdd(&degrees, &minutes, &angleDms, &ctxtReal39);
  realDivide(&seconds, const_10000, &seconds, &ctxtReal39);
  realAdd(&angleDms, &seconds, &angleDms, &ctxtReal39);

  if(sign == -1) {
    realSetNegativeSign(&angleDms);
  }

  realToReal34(&angleDms, angle34Dms);
}



uint32_t getInfiniteComplexAngle(real_t *x, real_t *y) {
  if(!realIsInfinite(x)) {
    if(realIsPositive(y)) {
      return 2; // 90°
    }
    else {
      return 6; // -90° or 270°
    }
  }

  if(!realIsInfinite(y)) {
    if(realIsPositive(x)) {
      return 0; // 0°
    }
    else {
      return 4; // -180° or 180°
    }
  }

  // At this point, x and y are infinite
  if(realIsPositive(x)) {
    if(realIsPositive(y)) {
      return 1; // 45°
    }
    else {
      return 7; // -45° or 315°
    }
  }

  // At this point, x is negative
  if(realIsPositive(y)) {
    return 3; // 135°
  }
  else {
    return 5; // -135° or 225°
  }
}



void setInfiniteComplexAngle(uint32_t angle, real_t *x, real_t *y) {
  switch(angle) {
    case 3:
    case 4:
    case 5: {
      realCopy(const_minusInfinity, x);
      break;
    }

    case 2:
    case 6: {
      realZero(x);
      break;
    }

    default: {
      realCopy(const_plusInfinity, x);
    }
  }

  switch(angle) {
    case 5:
    case 6:
    case 7: {
      realCopy(const_minusInfinity, y);
      break;
    }

    case 0:
    case 4: {
      realZero(y);
      break;
    }

    default: {
      realCopy(const_plusInfinity, y);
    }
  }
}



void real34FromDmsToDeg(const real34_t *angleDms, real34_t *angleDec) {
  real_t angle, degrees, minutes, seconds;

  real34ToReal(angleDms, &angle);
  realSetPositiveSign(&angle);

  decContextClearStatus(&ctxtReal34, DEC_Invalid_operation);
  realToIntegralValue(&angle, &degrees, DEC_ROUND_DOWN, &ctxtReal39);

  realSubtract(&angle, &degrees, &angle, &ctxtReal39);
  realMultiply(&angle, const_100, &angle, &ctxtReal39);

  realToIntegralValue(&angle, &minutes, DEC_ROUND_DOWN, &ctxtReal39);

  realSubtract(&angle, &minutes, &angle, &ctxtReal39);
  realMultiply(&angle, const_100, &seconds, &ctxtReal39);

  if(realCompareGreaterEqual(&seconds, const_60)) {
    realSubtract(&seconds, const_60, &seconds, &ctxtReal39);
    realAdd(&minutes, const_1, &minutes, &ctxtReal39);
  }

  if(realCompareGreaterEqual(&minutes, const_60)) {
    realSubtract(&minutes, const_60, &minutes, &ctxtReal39);
    realAdd(&degrees, const_1, &degrees, &ctxtReal39);
  }

  realDivide(&minutes, const_60,   &minutes, &ctxtReal39);
  realDivide(&seconds, const_3600, &seconds, &ctxtReal39);

  realAdd(&degrees, &minutes, &angle, &ctxtReal39);
  realAdd(&angle,   &seconds, &angle, &ctxtReal39);

  if(real34IsNegative(angleDms)) {
    realSetNegativeSign(&angle);
  }

  realToReal34(&angle, angleDec);
}



void real34FromDegToDms(const real34_t *angleDec, real34_t *angleDms) {
  real_t angle, degrees, minutes, seconds;

  real34ToReal(angleDec, &angle);
  realSetPositiveSign(&angle);

  realToIntegralValue(&angle, &degrees, DEC_ROUND_DOWN, &ctxtReal39);

  realSubtract(&angle, &degrees, &angle, &ctxtReal39);
  realMultiply(&angle, const_60, &angle, &ctxtReal39);

  realToIntegralValue(&angle, &minutes, DEC_ROUND_DOWN, &ctxtReal39);

  realSubtract(&angle, &minutes, &angle, &ctxtReal39);
  realMultiply(&angle, const_60, &seconds, &ctxtReal39);

  realDivide(&minutes, const_100,   &minutes, &ctxtReal39);
  realDivide(&seconds, const_10000, &seconds, &ctxtReal39);

  realAdd(&degrees, &minutes, &angle, &ctxtReal39);
  realAdd(&angle,   &seconds, &angle, &ctxtReal39);

  if(real34IsNegative(angleDec)) {
    realSetNegativeSign(&angle);
  }

  realToReal34(&angle, angleDms);

  checkDms34(angleDms);
}
