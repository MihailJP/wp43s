// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file apps/bugScreen.h
 */
#if !defined(BUGSCREEN_H)
  #define BUGSCREEN_H

  void bugScreen(const char *msg);

#endif // !BUGSCREEN_H
