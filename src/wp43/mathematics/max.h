// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/max.h
 */
#if !defined(MAX_H)
  #define MAX_H

  #include <stdint.h>

  void fnMax(uint16_t unusedButMandatoryParameter);

#endif // !MAX_H
