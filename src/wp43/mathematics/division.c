// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "mathematics/division.h"

#include "constantPointers.h"
#include "debug.h"
#include "error.h"
#include "conversionAngles.h"
#include "flags.h"
#include "fonts.h"
#include "integers.h"
#include "items.h"
#include "mathematics/matrix.h"
#include "registers.h"
#include "registerValueConversions.h"
#include <stdbool.h>

#include "wp43.h"

#if (EXTRA_INFO_ON_CALC_ERROR == 1)
  void divError(void);
#else // (EXTRA_INFO_ON_CALC_ERROR == 1)
  #define divError typeError
#endif // (EXTRA_INFO_ON_CALC_ERROR == 1)

TO_QSPI void (* const division[NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS][NUMBER_OF_DATA_TYPES_FOR_CALCULATIONS])(void) = {
// regX |    regY ==>   1            2            3            4            5         6         7            8            9             10
//      V               Long integer Real34       Complex34    Time         Date      String    Real34 mat   Complex34 m  Short integer Config data
/*  1 Long integer  */ {divLonILonI, divRealLonI, divCplxLonI, divTimeLonI, divError, divError, divRemaLonI, divCxmaLonI, divShoILonI,  divError},
/*  2 Real34        */ {divLonIReal, divRealReal, divCplxReal, divTimeReal, divError, divError, divRemaReal, divCxmaReal, divShoIReal,  divError},
/*  3 Complex34     */ {divLonICplx, divRealCplx, divCplxCplx, divError,    divError, divError, divRemaCplx, divCxmaCplx, divShoICplx,  divError},
/*  4 Time          */ {divLonITime, divRealTime, divError,    divTimeTime, divError, divError, divError,    divError,    divShoITime,  divError},
/*  5 Date          */ {divError,    divError,    divError,    divError,    divError, divError, divError,    divError,    divError,     divError},
/*  6 String        */ {divError,    divError,    divError,    divError,    divError, divError, divError,    divError,    divError,     divError},
/*  7 Real34 mat    */ {divLonIRema, divRealRema, divCplxRema, divError,    divError, divError, divRemaRema, divCxmaRema, divShoIRema,  divError},
/*  8 Complex34 mat */ {divLonICxma, divRealCxma, divCplxCxma, divError,    divError, divError, divRemaCxma, divCxmaCxma, divShoICxma,  divError},
/*  9 Short integer */ {divLonIShoI, divRealShoI, divCplxShoI, divTimeShoI, divError, divError, divRemaShoI, divCxmaShoI, divShoIShoI,  divError},
/* 10 Config data   */ {divError,    divError,    divError,    divError,    divError, divError, divError,    divError,    divError,     divError}
};

#if (EXTRA_INFO_ON_CALC_ERROR == 1)
  void divError(void) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide %s\nby %s",
        getRegisterDataTypeName(REGISTER_Y, true, false),
        getRegisterDataTypeName(REGISTER_X, true, false));
  }
#endif // (EXTRA_INFO_ON_CALC_ERROR == 1)



void fnDivide(uint16_t unusedButMandatoryParameter) {
  copySourceRegisterToDestRegister(REGISTER_X, REGISTER_L);

  division[getRegisterDataType(REGISTER_X)][getRegisterDataType(REGISTER_Y)]();

  adjustResult(REGISTER_X, true, true, REGISTER_X, REGISTER_Y, -1);
}



void divComplexComplex(const real_t *numerReal, const real_t *numerImag, const real_t *denomReal, const real_t *denomImag, real_t *quotientReal, real_t *quotientImag, realContext_t *realContext) {
  real_t realNumer, realDenom, a, b, c, d;

  realCopy(numerReal, &a);
  realCopy(numerImag, &b);
  realCopy(denomReal, &c);
  realCopy(denomImag, &d);

  if(realIsNaN(&a) || realIsNaN(&b) || realIsNaN(&c) || realIsNaN(&d)) {
    realCopy(const_NaN, quotientReal);
    realCopy(const_NaN, quotientImag);
    return;
  }

  if(realIsInfinite(&c) || realIsInfinite(&d)) {
    if(realIsInfinite(&a) || realIsInfinite(&b)) {
      realCopy(const_NaN, quotientReal);
      realCopy(const_NaN, quotientImag);
    }
    else {
      realZero(quotientReal);
      realZero(quotientImag);
    }
    return;
  }

  if(realIsInfinite(&a) && !realIsInfinite(&b)) {
    realZero(&b);
  }

  if(realIsInfinite(&b) && !realIsInfinite(&a)) {
    realZero(&a);
  }


  // Denominator
  realMultiply(&c, &c, &realDenom, realContext);                 // realDenom = c²
  realFMA(&d, &d, &realDenom, &realDenom, realContext);          // realDenom = c² + d²

  // real part
  realMultiply(&a, &c, &realNumer, realContext);                 // realNumer = a*c
  realFMA(&b, &d, &realNumer, &realNumer, realContext);          // realNumer = a*c + b*d
  realDivide(&realNumer, &realDenom, quotientReal, realContext); // realPart = (a*c + b*d) / (c² + d²) = realNumer / realDenom

  // imaginary part
  realMultiply(&b, &c, &realNumer, realContext);                 // realNumer = b*c
  realChangeSign(&a);                                            // a = -a
  realFMA(&a, &d, &realNumer, &realNumer, realContext);          // realNumer = b*c - a*d
  realDivide(&realNumer, &realDenom, quotientImag, realContext); // imagPart = (b*c - a*d) / (c² + d²) = realNumer / realDenom

}



void divRealComplex(const real_t *numerReal, const real_t *denomReal, const real_t *denomImag, real_t *quotientReal, real_t *quotientImag, realContext_t *realContext) {
  real_t a, c, d, denom;

  realCopy(numerReal, &a);
  realCopy(denomReal, &c);
  realCopy(denomImag, &d);

  if(realIsNaN(&a) || realIsNaN(&c) || realIsNaN(&d)) {
    realCopy(const_NaN, quotientReal);
    realCopy(const_NaN, quotientImag);
    return;
  }

  if(realIsInfinite(&c) || realIsInfinite(&d)) {
    realZero(quotientReal);
    realZero(quotientImag);
    return;
  }

  realMultiply(&c, &c, &denom, realContext);    // c²
  realFMA(&d, &d, &denom, &denom, realContext); // c² + d²

  // real part
  realMultiply(&a, &c, quotientReal, realContext);             // numer = a*c
  realDivide(quotientReal, &denom, quotientReal, realContext); // realPart  = (a*c) / (c² + d²) = numer / denom

  // imaginary part
  realChangeSign(&a);                                          // a = -a
  realMultiply(&a, &d, quotientImag, realContext);             // numer = -a*d
  realDivide(quotientImag, &denom, quotientImag, realContext); // imagPart  = -(a*d) / (c² + d²) = numer / denom
}



void divLonILonI(void) {
  longInteger_t x;

  convertLongIntegerRegisterToLongInteger(REGISTER_X, x);

  if(longIntegerIsZero(x)) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide a long integer by 0");
  }
  else {
    longInteger_t y, quotient, remainder;

    convertLongIntegerRegisterToLongInteger(REGISTER_Y, y);
    longIntegerInit(quotient);
    longIntegerInit(remainder);
    longIntegerDivideQuotientRemainder(y, x, quotient, remainder);

    if(longIntegerIsZero(remainder)) {
      convertLongIntegerToLongIntegerRegister(quotient, REGISTER_X);
    }
    else {
      real_t xIc, yIc;

      convertLongIntegerRegisterToReal(REGISTER_Y, &yIc, &ctxtReal39);
      convertLongIntegerRegisterToReal(REGISTER_X, &xIc, &ctxtReal39);
      reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);

      realDivide(&yIc, &xIc, &xIc, &ctxtReal39);
      convertRealToReal34ResultRegister(&xIc, REGISTER_X);
    }

    longIntegerFree(quotient);
    longIntegerFree(remainder);
    longIntegerFree(y);
  }

  longIntegerFree(x);
}



void divLonIShoI(void) {
  longInteger_t a, c;

  convertLongIntegerRegisterToLongInteger(REGISTER_Y, a);
  convertShortIntegerRegisterToLongIntegerRegister(REGISTER_X, REGISTER_X);
  convertLongIntegerRegisterToLongInteger(REGISTER_X, c);

  if(longIntegerIsZero(c)) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide a long integer by 0");
  }
  else {
    longIntegerDivideQuotientRemainder(a, c, a, c);
    convertLongIntegerToLongIntegerRegister(a, REGISTER_X);
  }

  longIntegerFree(a);
  longIntegerFree(c);
}



void divShoILonI(void) {
  longInteger_t a, c;

  convertShortIntegerRegisterToLongIntegerRegister(REGISTER_Y, REGISTER_Y);
  convertLongIntegerRegisterToLongInteger(REGISTER_Y, a);
  convertLongIntegerRegisterToLongInteger(REGISTER_X, c);

  if(longIntegerIsZero(c)) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide a short integer by 0");
  }
  else {
    longIntegerDivideQuotientRemainder(a, c, a, c);
    convertLongIntegerToLongIntegerRegister(a, REGISTER_X);
  }

  longIntegerFree(a);
  longIntegerFree(c);
}



void divLonIReal(void) {
  real_t y, x;

  setRegisterAngularMode(REGISTER_X, amNone);
  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);

  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(realIsZero(&y)) {
      if(getSystemFlag(FLAG_SPCRES)) {
        convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((realIsPositive(&y) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide a long integer by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
    realDivide(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
}



void divRealLonI(void) {
  real_t y, x;
  angularMode_t yAngularMode;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);

  if(realIsZero(&x)) {
    if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y))) {
      if(getSystemFlag(FLAG_SPCRES)) {
        convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide a real34 by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
    yAngularMode = getRegisterAngularMode(REGISTER_Y);

    if(yAngularMode == amNone) {
      realDivide(&y, &x, &x, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
    }
    else {
      convertAngleFromTo(&y, yAngularMode, currentAngularMode, &ctxtReal39);
      realDivide(&y, &x, &x, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
    }
  }
}



void divLonICplx(void) {
  real_t y, xReal, xImag;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &xReal);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_X), &xImag);

  divRealComplex(&y, &xReal, &xImag, &xReal, &xImag, &ctxtReal39);

  convertRealToReal34ResultRegister(&xReal, REGISTER_X);
  convertRealToImag34ResultRegister(&xImag, REGISTER_X);
}



void divCplxLonI(void) {
  real_t a, b, c;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &a);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_Y), &b);
  convertLongIntegerRegisterToReal(REGISTER_X, &c, &ctxtReal39);

  realDivide(&a, &c, &a, &ctxtReal39);
  realDivide(&b, &c, &b, &ctxtReal39);

  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  convertRealToReal34ResultRegister(&a, REGISTER_X);
  convertRealToImag34ResultRegister(&b, REGISTER_X);
}



void divTimeLonI(void) {
  real_t y, x;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtTime, REAL34_SIZE_IN_BYTES, amNone);

  if(realIsZero(&x)) {
    if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y))) {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34(const_NaN, REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide time by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);

    realDivide(&y, &x, &x, &ctxtReal39);
    realToReal34(&x, REGISTER_REAL34_DATA(REGISTER_X));
  }
}



void divLonITime(void) {
  convertTimeRegisterToReal34Register(REGISTER_X, REGISTER_X);
  divLonIReal();
}



void divTimeShoI(void) {
  real_t y, x;

  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtTime, REAL34_SIZE_IN_BYTES, amNone);

  if(realIsZero(&x)) {
    if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y))) {
      if(getSystemFlag(FLAG_SPCRES)) {
        convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide time by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);

    realDivide(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
}



void divShoITime(void) {
  convertTimeRegisterToReal34Register(REGISTER_X, REGISTER_X);
  divShoIReal();
}



void divTimeReal(void) {
  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y)) && real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      reallocateRegister(REGISTER_X, dtTime, REAL34_SIZE_IN_BYTES, amNone);
      convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide 0 by 0");
    }
  }

  else if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      reallocateRegister(REGISTER_X, dtTime, REAL34_SIZE_IN_BYTES, amNone);
      realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide a real34 by 0");
    }
  }

  else {
    real34_t x;
    angularMode_t xAngularMode;

    real34Copy(REGISTER_REAL34_DATA(REGISTER_X), &x);
    xAngularMode = getRegisterAngularMode(REGISTER_X);

    if(xAngularMode == amNone) { // time / real
      reallocateRegister(REGISTER_X, dtTime, REAL34_SIZE_IN_BYTES, amNone);
      real34Divide(REGISTER_REAL34_DATA(REGISTER_Y), &x, REGISTER_REAL34_DATA(REGISTER_X));
    }
    else { // time / angle
      divError();
    }
  }
}



void divRealTime(void) {
  convertTimeRegisterToReal34Register(REGISTER_X, REGISTER_X);
  divRealReal();
}



void divTimeTime(void) {
  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y)) && real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide 0 by 0");
    }
  }

  else if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide time by 0");
    }
  }

  else {
    real34_t b;

    real34Copy(REGISTER_REAL34_DATA(REGISTER_X), &b);
    reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);
    real34Divide(REGISTER_REAL34_DATA(REGISTER_Y), &b, REGISTER_REAL34_DATA(REGISTER_X));
    setRegisterAngularMode(REGISTER_X, amNone);
  }
}



void divRemaLonI(void) {
  real34Matrix_t matrix, res;
  real_t x;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  if(!getSystemFlag(FLAG_SPCRES) && realIsZero(&x)) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }

  else {
    linkToRealMatrixRegister(REGISTER_Y, &matrix);
    _divideRealMatrix(&matrix, &x, &res, &ctxtReal39);
    convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
    realMatrixFree(&res);
  }
}



void divLonIRema(void) {
  real34Matrix_t matrix, res;
  real_t         y;
  bool           divZeroOccurs = false;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  linkToRealMatrixRegister(REGISTER_X, &matrix);
  if(!getSystemFlag(FLAG_SPCRES)) {
    const uint16_t rows = matrix.header.matrixRows;
    const uint16_t cols = matrix.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      if(real34IsZero(&matrix.matrixElements[i])) {
        divZeroOccurs = true;
      }
    }
  }

  if(divZeroOccurs) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }

  else {
    _divideByRealMatrix(&y, &matrix, &res, &ctxtReal39);
    convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
    realMatrixFree(&res);
  }
}



void divRemaRema(void) {
  real34Matrix_t y, x, res;

  linkToRealMatrixRegister(REGISTER_Y, &y);
  linkToRealMatrixRegister(REGISTER_X, &x);

  if(y.header.matrixColumns != x.header.matrixRows || y.header.matrixColumns != x.header.matrixColumns || x.header.matrixRows != x.header.matrixColumns) {
    displayCalcErrorMessage(ERROR_MATRIX_MISMATCH, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide %d" STD_CROSS "%d-matrix and %d" STD_CROSS "%d-matrix",
        y.header.matrixRows, y.header.matrixColumns,
        x.header.matrixRows, x.header.matrixColumns);
  }
  else {
    divideRealMatrices(&y, &x, &res);
    if(res.matrixElements) {
      convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
      realMatrixFree(&res);
    }
    else {
      displayCalcErrorMessage(ERROR_SINGULAR_MATRIX, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide by a singular matrix");
    }
  }
}



void divRemaCxma(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_Y, REGISTER_Y);
  divCxmaCxma();
}



void divRemaShoI(void) {
  real34Matrix_t matrix, res;
  real_t x;

  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  if(!getSystemFlag(FLAG_SPCRES) && realIsZero(&x)) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }

  else {
    linkToRealMatrixRegister(REGISTER_Y, &matrix);
    _divideRealMatrix(&matrix, &x, &res, &ctxtReal39);
    convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
    realMatrixFree(&res);
  }
}



void divShoIRema(void) {
  real34Matrix_t matrix, res;
  real_t         y;
  bool           divZeroOccurs = false;

  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  linkToRealMatrixRegister(REGISTER_X, &matrix);
  if(!getSystemFlag(FLAG_SPCRES)) {
    const uint16_t rows = matrix.header.matrixRows;
    const uint16_t cols = matrix.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      if(real34IsZero(&matrix.matrixElements[i])) {
        divZeroOccurs = true;
      }
    }
  }

  if(divZeroOccurs) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }

  else {
    _divideByRealMatrix(&y, &matrix, &res, &ctxtReal39);
    convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
    realMatrixFree(&res);
  }
}



void divRemaReal(void) {
  real34Matrix_t matrix;
  if(!getSystemFlag(FLAG_SPCRES) && real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }
  else if(getRegisterAngularMode(REGISTER_X) == amNone) {
    linkToRealMatrixRegister(REGISTER_Y, &matrix);
    divideRealMatrix(&matrix, REGISTER_REAL34_DATA(REGISTER_X), &matrix);
    convertReal34MatrixToReal34MatrixRegister(&matrix, REGISTER_X);
  }
  else {
    elementwiseRemaReal(divRealReal);
  }
}



void divRealRema(void) {
  real34Matrix_t matrix, res;
  bool           divZeroOccurs = false;

  linkToRealMatrixRegister(REGISTER_X, &matrix);
  if(!getSystemFlag(FLAG_SPCRES)) {
    const uint16_t rows = matrix.header.matrixRows;
    const uint16_t cols = matrix.header.matrixColumns;
    int32_t i;

    for(i = 0; i < cols * rows; ++i) {
      if(real34IsZero(&matrix.matrixElements[i])) {
        divZeroOccurs = true;
      }
    }
  }

  if(divZeroOccurs) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide by 0");
  }

  else if(getRegisterAngularMode(REGISTER_Y) == amNone) {
    divideByRealMatrix(REGISTER_REAL34_DATA(REGISTER_Y), &matrix, &res);
    convertReal34MatrixToReal34MatrixRegister(&res, REGISTER_X);
    realMatrixFree(&res);
  }

  else {
    elementwiseRealRema(divRealReal);
  }
}



void divRemaCplx(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_Y, REGISTER_Y);
  divCxmaCplx();
}



void divCplxRema(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_X, REGISTER_X);
  divCplxCxma();
}



void divCxmaLonI(void) {
  complex34Matrix_t matrix, res;
  real_t x;

  convertLongIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  linkToComplexMatrixRegister(REGISTER_Y, &matrix);
  _divideComplexMatrix(&matrix, &x, const_0, &res, &ctxtReal39);
  convertComplex34MatrixToComplex34MatrixRegister(&res, REGISTER_X);
  complexMatrixFree(&res);
}



void divLonICxma(void) {
  complex34Matrix_t matrix, res;
  real_t y;

  convertLongIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  linkToComplexMatrixRegister(REGISTER_X, &matrix);
  _divideByComplexMatrix(&y, const_0, &matrix, &res, &ctxtReal39);
  convertComplex34MatrixToComplex34MatrixRegister(&res, REGISTER_X);
  complexMatrixFree(&res);
}



void divCxmaRema(void) {
  convertReal34MatrixRegisterToComplex34MatrixRegister(REGISTER_X, REGISTER_X);
  divCxmaCxma();
}



void divCxmaCxma(void) {
  complex34Matrix_t y, x, res;

  linkToComplexMatrixRegister(REGISTER_Y, &y);
  linkToComplexMatrixRegister(REGISTER_X, &x);

  if(y.header.matrixColumns != x.header.matrixRows || y.header.matrixColumns != x.header.matrixColumns || x.header.matrixRows != x.header.matrixColumns) {
    displayCalcErrorMessage(ERROR_MATRIX_MISMATCH, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide %d" STD_CROSS "%d-matrix and %d" STD_CROSS "%d-matrix",
        y.header.matrixRows, y.header.matrixColumns,
        x.header.matrixRows, x.header.matrixColumns);
  }
  else {
    divideComplexMatrices(&y, &x, &res);
    if(res.matrixElements) {
      convertComplex34MatrixToComplex34MatrixRegister(&res, REGISTER_X);
      complexMatrixFree(&res);
    }
    else {
      displayCalcErrorMessage(ERROR_SINGULAR_MATRIX, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide by a singular matrix");
    }
  }
}



void divCxmaShoI(void) {
  convertShortIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
  divCxmaReal();
}



void divShoICxma(void) {
  convertShortIntegerRegisterToReal34Register(REGISTER_Y, REGISTER_Y);
  divRealCxma();
}



void divCxmaReal(void) {
  complex34Matrix_t matrix;
  if(getRegisterAngularMode(REGISTER_X) == amNone) {
    linkToComplexMatrixRegister(REGISTER_Y, &matrix);
    divideComplexMatrix(&matrix, REGISTER_REAL34_DATA(REGISTER_X), const34_0, &matrix);
    convertComplex34MatrixToComplex34MatrixRegister(&matrix, REGISTER_X);
  }
  else {
    elementwiseCxmaReal(divCplxReal);
  }
}



void divRealCxma(void) {
  complex34Matrix_t matrix;
  if(getRegisterAngularMode(REGISTER_Y) == amNone) {
    linkToComplexMatrixRegister(REGISTER_X, &matrix);
    divideByComplexMatrix(REGISTER_REAL34_DATA(REGISTER_Y), const34_0, &matrix, &matrix);
  }
  else {
    elementwiseRealCxma(divRealCplx);
  }
}



void divCxmaCplx(void) {
  complex34Matrix_t matrix;
  linkToComplexMatrixRegister(REGISTER_Y, &matrix);
  divideComplexMatrix(&matrix, REGISTER_REAL34_DATA(REGISTER_X), REGISTER_IMAG34_DATA(REGISTER_X), &matrix);
  convertComplex34MatrixToComplex34MatrixRegister(&matrix, REGISTER_X);
}



void divCplxCxma(void) {
  complex34Matrix_t matrix;
  linkToComplexMatrixRegister(REGISTER_X, &matrix);
  divideByComplexMatrix(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_IMAG34_DATA(REGISTER_Y), &matrix, &matrix);
}



void divShoIShoI(void) {
  int16_t sign;
  uint64_t value;

  convertShortIntegerRegisterToUInt64(REGISTER_X, &sign, &value);
  if(value == 0) {
    displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
    errorMoreInfo("cannot divide a short integer by 0");
  }
  else {
    *(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) = WP34S_intDivide(*(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)), *(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)));
    setRegisterShortIntegerBase(REGISTER_X, getRegisterShortIntegerBase(REGISTER_Y));
  }
}



void divShoIReal(void) {
  real_t y, x;

  setRegisterAngularMode(REGISTER_X, amNone);
  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);

  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(realIsZero(&y)) {
      if(getSystemFlag(FLAG_SPCRES)) {
        convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((realIsPositive(&y) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide a short integer by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
    realDivide(&y, &x, &x, &ctxtReal39);
    convertRealToReal34ResultRegister(&x, REGISTER_X);
  }
}



void divRealShoI(void) {
  real_t y, x;
  angularMode_t yAngularMode;

  convertShortIntegerRegisterToReal(REGISTER_X, &x, &ctxtReal39);
  reallocateRegister(REGISTER_X, dtReal34, REAL34_SIZE_IN_BYTES, amNone);

  if(realIsZero(&x)) {
    if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y))) {
      if(getSystemFlag(FLAG_SPCRES)) {
        convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide 0 by 0");
      }
    }
    else {
      if(getSystemFlag(FLAG_SPCRES)) {
        realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
      }
      else {
        displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
        errorMoreInfo("cannot divide a real34 by 0");
      }
    }
  }

  else {
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
    yAngularMode = getRegisterAngularMode(REGISTER_Y);

    if(yAngularMode == amNone) {
      realDivide(&y, &x, &x, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
    }
    else {
      convertAngleFromTo(&y, yAngularMode, currentAngularMode, &ctxtReal39);
      realDivide(&y, &x, &x, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
    }
  }
}



void divShoICplx(void) {
  real_t y, xReal, xImag;

  convertShortIntegerRegisterToReal(REGISTER_Y, &y, &ctxtReal39);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &xReal);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_X), &xImag);

  divRealComplex(&y, &xReal, &xImag, &xReal, &xImag, &ctxtReal39);

  convertRealToReal34ResultRegister(&xReal, REGISTER_X);
  convertRealToImag34ResultRegister(&xImag, REGISTER_X);
}



void divCplxShoI(void) {
  convertShortIntegerRegisterToReal34Register(REGISTER_X, REGISTER_X);
  real34Divide(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_Y)); // real part
  real34Divide(REGISTER_IMAG34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_IMAG34_DATA(REGISTER_Y)); // imaginary part
  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  complex34Copy(REGISTER_COMPLEX34_DATA(REGISTER_Y), REGISTER_COMPLEX34_DATA(REGISTER_X));
}



void divRealReal(void) {
  if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_Y)) && real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      convertRealToReal34ResultRegister(const_NaN, REGISTER_X);
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide 0 by 0");
    }
  }

  else if(real34IsZero(REGISTER_REAL34_DATA(REGISTER_X))) {
    if(getSystemFlag(FLAG_SPCRES)) {
      realToReal34((real34IsPositive(REGISTER_REAL34_DATA(REGISTER_Y)) ? const_plusInfinity : const_minusInfinity), REGISTER_REAL34_DATA(REGISTER_X));
    }
    else {
      displayCalcErrorMessage(ERROR_ARG_EXCEEDS_FUNCTION_DOMAIN, ERR_REGISTER_LINE, REGISTER_X);
      errorMoreInfo("cannot divide a real34 by 0");
    }
  }

  else {
    real_t y, x;
    angularMode_t yAngularMode, xAngularMode;

    real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
    yAngularMode = getRegisterAngularMode(REGISTER_Y);
    real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &x);
    xAngularMode = getRegisterAngularMode(REGISTER_X);

    if(yAngularMode != amNone && xAngularMode != amNone) { // angle / angle
      convertAngleFromTo(&x, xAngularMode, yAngularMode, &ctxtReal39);
      realDivide(&y, &x, &x, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, amNone);
    }
    else if(yAngularMode == amNone) { // real / (real or angle)
      real34Divide(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_X));
      setRegisterAngularMode(REGISTER_X, amNone);
    }
    else { // angle / real
      realDivide(&y, &x, &x, &ctxtReal39);

      convertAngleFromTo(&x, yAngularMode, currentAngularMode, &ctxtReal39);
      convertRealToReal34ResultRegister(&x, REGISTER_X);
      setRegisterAngularMode(REGISTER_X, currentAngularMode);
    }
  }
}



void divRealCplx(void) {
  real_t y, xReal, xImag;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &y);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &xReal);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_X), &xImag);

  divRealComplex(&y, &xReal, &xImag, &xReal, &xImag, &ctxtReal39);

  convertRealToReal34ResultRegister(&xReal, REGISTER_X);
  convertRealToImag34ResultRegister(&xImag, REGISTER_X);
}



void divCplxReal(void) {
  real34Divide(REGISTER_REAL34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_REAL34_DATA(REGISTER_Y)); // real part
  real34Divide(REGISTER_IMAG34_DATA(REGISTER_Y), REGISTER_REAL34_DATA(REGISTER_X), REGISTER_IMAG34_DATA(REGISTER_Y)); // imaginary part
  reallocateRegister(REGISTER_X, dtComplex34, COMPLEX34_SIZE_IN_BYTES, amNone);
  complex34Copy(REGISTER_COMPLEX34_DATA(REGISTER_Y), REGISTER_COMPLEX34_DATA(REGISTER_X));
}



void divCplxCplx(void) {
  real_t yReal, yImag, xReal, xImag;

  real34ToReal(REGISTER_REAL34_DATA(REGISTER_Y), &yReal);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_Y), &yImag);
  real34ToReal(REGISTER_REAL34_DATA(REGISTER_X), &xReal);
  real34ToReal(REGISTER_IMAG34_DATA(REGISTER_X), &xImag);

  divComplexComplex(&yReal, &yImag, &xReal, &xImag, &xReal, &xImag, &ctxtReal39);

  convertRealToReal34ResultRegister(&xReal, REGISTER_X);
  convertRealToImag34ResultRegister(&xImag, REGISTER_X);
}
