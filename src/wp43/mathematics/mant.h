// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/mant.h
 */
#if !defined(MANT_H)
  #define MANT_H

  #include "defines.h"
  #include <stdint.h>

  void fnMant   (uint16_t unusedButMandatoryParameter);

  #if (EXTRA_INFO_ON_CALC_ERROR == 1)
    void mantError(void);
  #else // (EXTRA_INFO_ON_CALC_ERROR != 1)
    #define mantError typeError
  #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)

  void mantLonI (void);
  void mantReal (void);

#endif // !MANT_H
