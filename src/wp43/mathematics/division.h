// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/division.h
 */
#if !defined(DIVISION_H)
  #define DIVISION_H

  #include "defines.h"
  #include "realType.h"
  #include <stdint.h>

  /**
   * regX ==> regL and regY ÷ regX ==> regX
   * Drops Y, enables stack lift and refreshes the stack
   *
   * \param[in] unusedButMandatoryParameter
   */
  void fnDivide(uint16_t unusedButMandatoryParameter);

  void divComplexComplex(const real_t *numerReal, const real_t *numerImag, const real_t *denomReal, const real_t *denomImag,    real_t *quotientReal, real_t *quotientImag, realContext_t *realContext);
  void divRealComplex   (const real_t *numer,     const real_t *denomReal, const real_t *denomImag,       real_t *quotientReal, real_t *quotientImag,                       realContext_t *realContext);

  //      RegYRegX
  void divLonILonI(void);
  void divLonIRema(void);
  void divLonICxma(void);
  void divLonIShoI(void);
  void divLonIReal(void);
  void divLonICplx(void);
  void divLonITime(void);

  //      RegYRegX
  void divTimeLonI(void);
  void divTimeShoI(void);
  void divTimeReal(void);
  void divTimeTime(void);

  //      RegYRegX
  void divRemaLonI(void);
  void divRemaRema(void);
  void divRemaCxma(void);
  void divRemaShoI(void);
  void divRemaReal(void);
  void divRemaCplx(void);

  //      RegYRegX
  void divCxmaLonI(void);
  void divCxmaRema(void);
  void divCxmaCxma(void);
  void divCxmaShoI(void);
  void divCxmaReal(void);
  void divCxmaCplx(void);

  //      RegYRegX
  void divShoILonI(void);
  void divShoIRema(void);
  void divShoICxma(void);
  void divShoIShoI(void);
  void divShoIReal(void);
  void divShoICplx(void);
  void divShoITime(void);

  //      RegYRegX
  void divRealLonI(void);
  void divRealRema(void);
  void divRealCxma(void);
  void divRealShoI(void);
  void divRealReal(void);
  void divRealCplx(void);
  void divRealTime(void);

  //      RegYRegX
  void divCplxLonI(void);
  void divCplxRema(void);
  void divCplxCxma(void);
  void divCplxShoI(void);
  void divCplxReal(void);
  void divCplxCplx(void);

#endif // !DIVISION_H
