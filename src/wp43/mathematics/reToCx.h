// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/reToCx.h
 */
#if !defined(RETOCX_H)
  #define RETOCX_H

  #include <stdint.h>

  void fnReToCx(uint16_t unusedButMandatoryParameter);

#endif // !RETOCX_H
