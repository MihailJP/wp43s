// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/sqrt.h
 */
#if !defined(SQUAREROOT_H)
  #define SQUAREROOT_H

  #include "defines.h"
  #include "realType.h"
  #include <stdint.h>

  void fnSquareRoot(uint16_t unusedButMandatoryParameter);

  #if (EXTRA_INFO_ON_CALC_ERROR == 1)
    void sqrtError   (void);
  #else // (EXTRA_INFO_ON_CALC_ERROR != 1)
    #define sqrtError typeError
  #endif // (EXTRA_INFO_ON_CALC_ERROR == 1)

  void sqrtLonI    (void);
  void sqrtRema    (void);
  void sqrtCxma    (void);
  void sqrtShoI    (void);
  void sqrtReal    (void);
  void sqrtCplx    (void);
  void sqrtComplex (const real_t *real, const real_t *imag, real_t *resReal, real_t *resImag, realContext_t *realContext);

  void real34SquareRoot(const real34_t *x, real34_t *res);

#endif // !SQUAREROOT_H
