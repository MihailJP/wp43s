// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/bn.h
 */
#if !defined(BN_H)
  #define BN_H

  #include <stdint.h>

  void fnBn    (uint16_t unusedButMandatoryParameter);
  void fnBnStar(uint16_t unusedButMandatoryParameter);

#endif // !BN_H
