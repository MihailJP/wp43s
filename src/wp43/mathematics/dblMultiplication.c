// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "mathematics/dblMultiplication.h"

#include "debug.h"
#include "error.h"
#include "flags.h"
#include "fonts.h"
#include "integers.h"
#include "registers.h"
#include "registerValueConversions.h"

#include "wp43.h"

void fnDblMultiply(uint16_t unusedButMandatoryParameter) {
  longInteger_t y, x, ans, wd;
  int32_t base;
  const uint8_t sim = shortIntegerMode;

  if(getRegisterDataType(REGISTER_X) != dtShortInteger) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_T);
    errorMoreInfo("the input type %s is not allowed for DBL" STD_CROSS "!", getDataTypeName(getRegisterDataType(REGISTER_X), false, false));
    return;
  }
  if(getRegisterDataType(REGISTER_Y) != dtShortInteger) {
    displayCalcErrorMessage(ERROR_INVALID_DATA_TYPE_FOR_OP, ERR_REGISTER_LINE, REGISTER_T);
    errorMoreInfo("the input type %s is not allowed for DBL" STD_CROSS "!", getDataTypeName(getRegisterDataType(REGISTER_Y), false, false));
    return;
  }

  if(!saveLastX()) {
    return;
  }

  longIntegerInit(wd);
  longInteger2Pow(shortIntegerWordSize, wd);

  convertShortIntegerRegisterToLongInteger(REGISTER_Y, y);
  convertShortIntegerRegisterToLongInteger(REGISTER_X, x);
  base = getRegisterShortIntegerBase(REGISTER_Y);

  longIntegerInit(ans);
  longIntegerMultiply(y, x, ans);
  if(sim == SIM_1COMPL) {
    if((*(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) & shortIntegerSignBit) != (*(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) & shortIntegerSignBit)) {
      longIntegerSubtractUInt(ans, 1u, ans);
    }
    shortIntegerMode = SIM_2COMPL;
  }
  else if(sim == SIM_SIGNMT) {
    longIntegerSetPositiveSign(ans);
    shortIntegerMode = SIM_UNSIGN;
  }

  longIntegerModulo(ans, wd, y);
  longIntegerSubtract(ans, y, x);
  longIntegerDivide(x, wd, x);

  if(sim == SIM_SIGNMT) {
    shortIntegerMode = SIM_SIGNMT;
    if((*(REGISTER_SHORT_INTEGER_DATA(REGISTER_Y)) & shortIntegerSignBit) != (*(REGISTER_SHORT_INTEGER_DATA(REGISTER_X)) & shortIntegerSignBit)) {
      longIntegerSetNegativeSign(x);
    }
  }

  convertLongIntegerToShortIntegerRegister(y, base, REGISTER_Y);
  convertLongIntegerToShortIntegerRegister(x, base, REGISTER_X);

  shortIntegerMode = sim;

  longIntegerFree(x);
  longIntegerFree(y);
  longIntegerFree(ans);
  longIntegerFree(wd);

  clearSystemFlag(FLAG_OVERFLOW);
}
