// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "hal/timer.h"

#pragma GCC diagnostic ignored "-Wunused-parameter"

void timerReset(void) {
}



void timerConfig(timerId_t nr, timerCallback_t func) {
}



void timerStart(timerId_t nr, uint16_t param, uint32_t time) {
}



void timerStop(timerId_t nr) {
}



bool timerIsRunning(timerId_t nr) {
  return false;
}
